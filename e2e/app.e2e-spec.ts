import { Ng4BasicPage } from './app.po';

describe('ng4-basic App', () => {
  let page: Ng4BasicPage;

  beforeEach(() => {
    page = new Ng4BasicPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
