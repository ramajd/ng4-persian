import {NgModule} from '@angular/core';

import {PJalaliPipe, PJalaliFromNowPipe} from './pipes/p-jalali.pipe';
import {PNumberPipe} from './pipes/p-number.pipe';
import {PDigitWordsPipe} from './pipes/p-digit-words.pipe';
import {PDayOfWeekPipe} from './pipes/p-day-of-week.pipe';
import {ArabicToPersianDirective} from './directives/arabic-to-persian.directive';
import {PNumberToENumberDirective} from './directives/pNumber-to-eNumber.directive';

@NgModule({
  imports: [],
  declarations: [
    PJalaliPipe,
    PJalaliFromNowPipe,
    PNumberPipe,
    PDigitWordsPipe,
    PDayOfWeekPipe,
    ArabicToPersianDirective,
    PNumberToENumberDirective],
  exports: [
    PJalaliPipe,
    PJalaliFromNowPipe,
    PNumberPipe,
    PDigitWordsPipe,
    PDayOfWeekPipe,
    ArabicToPersianDirective,
    PNumberToENumberDirective
  ]
})
export class Ng4PersianModule {
}
