export {PJalaliPipe, PJalaliFromNowPipe} from './pipes/p-jalali.pipe';
export {PNumberPipe} from './pipes/p-number.pipe';
export {PDigitWordsPipe} from './pipes/p-digit-words.pipe';
export {PDayOfWeekPipe} from './pipes/p-day-of-week.pipe';
export * from './ng4-persian.module'
