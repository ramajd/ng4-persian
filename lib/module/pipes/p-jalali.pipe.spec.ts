import { PJalaliPipe } from './p-jalali.pipe';

describe('PJalaliPipe', () => {
  it('create an instance', () => {
    const pipe = new PJalaliPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms 2017/6/22 to 1396/4/1', () => {
    const pipe = new PJalaliPipe();
    expect(pipe.transform(new Date(2017, 5, 22))).toEqual('1396/4/1');
  });
});
