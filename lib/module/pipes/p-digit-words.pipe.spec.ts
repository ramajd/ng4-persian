import { PDigitWordsPipe } from './p-digit-words.pipe';

describe('PDigitWordsPipe', () => {
  it('create an instance', () => {
    const pipe = new PDigitWordsPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms 100 to صد', () => {
    const pipe = new PDigitWordsPipe();
    expect(pipe.transform(100)).toEqual('صد');
  });

  it('transforms 1216 to هزار و دویست و شانزده', () => {
    const pipe = new PDigitWordsPipe();
    expect(pipe.transform(1216)).toEqual('هزار و دویست و شانزده');
  });

});
