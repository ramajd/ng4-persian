import { PDayOfWeekPipe } from './p-day-of-week.pipe'

describe('PDayOfWeekPipe', () => {
  it('create an instance', () => {
    const pipe = new PDayOfWeekPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms sun to یک‌شنبه', () => {
    const pipe = new PDayOfWeekPipe();
    expect(pipe.transform('sun')).toEqual('یک‌شنبه');
  });

  it('transforms FriDaY to جمعه', () => {
    const pipe = new PDayOfWeekPipe();
    expect(pipe.transform('FriDaY')).toEqual('جمعه');
  });

});
