import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'jalali-moment';

@Pipe({
  name: 'pJalali'
})
export class PJalaliPipe implements PipeTransform {
  constructor() {
    moment.loadPersian();
  }

  transform(day: any, format: any = 'jYYYY/jM/jD'): any {
    try {
      const MomentDate = moment(day);
      if (MomentDate.format) {
        return MomentDate.format(format);
      }
      return day;
    } catch (e) {
      return '';
    }
  }

}

@Pipe({
  name: 'pJalaliFromNow'
})
export class PJalaliFromNowPipe implements PipeTransform {
  constructor() {
    moment.loadPersian();
  }

  transform(day: any, format: any = 'jYYYY/jM/jD'): any {
    try {
      const MomentDate = moment(day);
      if (MomentDate.format) {
        return MomentDate.fromNow();
      }
      return day;
    } catch (e) {
      return '';
    }
  }

}
