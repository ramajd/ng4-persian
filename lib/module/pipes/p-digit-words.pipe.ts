import { Pipe, PipeTransform } from '@angular/core';
import { Persian } from '../persian'

@Pipe({
  name: 'pDigitWords'
})
export class PDigitWordsPipe implements PipeTransform {

  transform(str: any, args?: any): any {
    if ((str != null) && str.toString().trim() !== '') {
      return Persian.numberToPersianDigitWords(str);
    } else {
      return '';
    }
  }

}
