
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pDayOfWeek'
})
export class PDayOfWeekPipe implements PipeTransform {

  private shortNameDict = {
    'sat': 'شنبه',
    'sun': 'یک‌شنبه',
    'mon': 'دوشنبه',
    'tue': 'سه‌شنبه',
    'wed': 'چهارشنبه',
    'thr': 'پنج‌شنبه',
    'fri': 'جمعه'
  };
  private fullNameDict = {
    'saturday': this.shortNameDict['sat'],
    'sunday': this.shortNameDict['sun'],
    'monday': this.shortNameDict['mon'],
    'tuesday': this.shortNameDict['tue'],
    'wednesday': this.shortNameDict['wed'],
    'thursday': this.shortNameDict['thr'],
    'friday': this.shortNameDict['fri']
  };

  constructor() { }

  transform(val: string): string {
    val = val.toLowerCase();
    return (this.shortNameDict[val]
      ? this.shortNameDict[val]
      : (this.fullNameDict[val]
        ? this.fullNameDict[val]
        : val));
  }
}
