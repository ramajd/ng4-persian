import {Directive, HostListener, Renderer2, ElementRef} from '@angular/core';
import * as persianJs from 'persianjs/persian.js'
import {Persian} from '../persian';

@Directive({
  selector: '[pNumberToENumber]'
})
export class PNumberToENumberDirective {

  constructor(private renderer: Renderer2,
              private el: ElementRef) {
  }

  @HostListener('input')
  onKeyUp() {
    const correctValue = Persian.persianToEnglishNumber(this.el.nativeElement.value);
    this.el.nativeElement.value = correctValue || '';
  }

}
