import {Directive, HostListener, Renderer2, ElementRef} from '@angular/core';
import * as persianJs from 'persianjs/persian.js'

@Directive({
  selector: '[arabicToPersian]'
})
export class ArabicToPersianDirective {

  constructor(private renderer: Renderer2,
              private el: ElementRef) {
  }

  @HostListener('keyup')
  onKeyUp() {
    try {
      const correctValue = persianJs(this.el.nativeElement.value).arabicChar();
      this.el.nativeElement.value = correctValue;
    } catch (e) {
    }
  }

}
