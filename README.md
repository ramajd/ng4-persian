# Ng4Persian 
 Persian Language tools for angular 4 and angular 2, based on [angular-cli](https://github.com/angular/angular-cli) and [Ng4Basic](https://gitlab.com/bitbeter/ng4-basic) which is basic project based on angular-cli that you can use for creating your own libraries on it.

# Installation
```bash
npm install -g ng4-persian
```

# Usage

## Pipes
### `pJalali` - `Pipe`
this pipe will transform date object to jalali (Shamsi) format date.

#### Example

##### In angular component
```typescript
<!-- 2017/6/22 (month start from zero in Date object) -->
day = new Date(2017, 5, 22);
```

##### In html
```html
<p [innerText]="day | pJalali"></p>
```

##### Result
```
1396/4/1
```

### `pNumber` - `Pipe`
this pipe will transform english number to persian number.

#### Example

##### In angular component
```typescript
number = 123;
```

##### In html
```html
<p [innerText]="number | pNumber"></p>
```

##### Result
```
۱۲۳
```

### `pDigitWords` - `Pipe`
this pipe will transform english number to persian words.

#### Example

##### In angular component
```typescript
number = 123;
```

##### In html
```html
<p [innerText]="number | pDigitWords"></p>
```

##### Result
```
صد و بیست و سه
```

# Contributing
This is a open-source project. Fork the project, complete the code and send pull request.

# Thanks Note
`pJalali` pipe based on the [jalali-moment](https://github.com/fingerpich/jalali-moment) project, and [persianJs](https://github.com/usablica/persian.js) and
[angular-persian](https://github.com/mohebifar/angular-persian) ideas, which are angularJs plugins, used for creating `pNumber` and `pDigitWords` pipes.

# Roadmap
1. Complete README.md
2. `pDigitWords` pipe Speed Up
3. Write and Complete Example app in Project